import re

name = input()
correct = re.findall('^[_a-zA-Z]+[_0-9a-zA-Z]*$', name)
print(len(correct) != 0)