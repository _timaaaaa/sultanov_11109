def rational(f):
    def wrapper(first, second):
        if not isinstance(second, RationalFraction):
            second = RationalFraction(second)
        return f(first, second)
    return wrapper


class RationalFraction:  # + - * / reduce
    def __init__(self, numerator = 0, denominator = 1):
        if isinstance(numerator, str):  # строка в числителе
            if '/' in numerator:  # дробь
                [numstr, denomstr] = numerator.split('/')
                numerator = int(numstr)
                denominator = int(denomstr)
            else:
                s = numerator.strip()
                i = s.find('.')
                if i >= 0:
                    numerator = int(s[:i] + s[i + 1:])
                    denominator = 10 ** (len(s) - i - 1)
                else:
                    numerator = int(numerator)
        self.numerator = numerator
        self.denominator = denominator

    def __repr__(self):
        return "RationalFraction('{}')".format(self)

    def __str__(self):
        return '%s/%s' % (self.numerator, self.denominator)

    def __int__(self):
        return self.numerator // self.denominator

    def __float__(self):
        return self.numerator / self.denominator

    def reduce(self):
        from math import gcd

        gcd_ = gcd(self.numerator, self.denominator)
        return RationalFraction(self.numerator // gcd_, self.denominator // gcd_)

    @rational
    def __eq__(self, other):
        self.reduce()
        other.reduce()
        return self.numerator == other.numerator and self.denominator == other.denominator

    @rational
    def __lt__(self, other):
        self.reduce()
        other.reduce()
        return self.numerator <= other.numerator and self.denominator >= other.denominator

    @rational
    def __gt__(self, other):
        self.reduce()
        other.reduce()
        return self.numerator >= other.numerator and self.denominator <= other.denominator

    @rational
    def __add__(self, other):
        return RationalFraction(self.numerator * other.denominator + self.denominator * other.numerator,
                                self.denominator * other.denominator)

    @rational
    def __sub__(self, other):
        return RationalFraction(self.numerator * other.denominator - self.denominator * other.numerator,
                                self.denominator * other.denominator)

    @rational
    def __mul__(self, other):
        return RationalFraction(self.numerator * other.numerator, self.denominator * other.denominator)

    @rational
    def __truediv__(self, other):
        return RationalFraction(self.numerator * other.denominator,
                                self.denominator * other.numerator)