def reverse(s):
    return s[::-1]

def palindrome(s):
    rev = reverse(s)

    if (s == rev):
        return True

s = "ogfdkvgk"
good = palindrome(s)
print(palindrome(s))