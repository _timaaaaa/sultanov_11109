def F(x, a):
    st = 0
    end = len(x) - 1
    while st <= end:
        mid = (st + end) // 2
        if a == x[mid]:
            return mid
        if a < x[mid]:
            end = mid - 1
        else:
            st = mid + 1

    return False

print(F([1, 2, 3, 4, 5, 6, 7, 8, 9], 5))