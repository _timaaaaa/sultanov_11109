import re

e = input('Введите строку в формате HH:MM:SS: ')
match = re.fullmatch(r'\d\d:\d\d:\d\d', e)
if match:
    if int(e[0:2]) < 24 and int(e[3:5]) < 60 and int(e[6:8]) < 60:
        print('Соответствует формату')
    else:
        print('Не соответствует формату')
else:
    print('Не соответствует формату')

