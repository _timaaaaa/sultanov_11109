class Directors():
    def __init__(self, id, name, mov):
        self.id = id
        self.name = name
        self.mov = mov


class Actors():
    def __init__(self, id, name, exp):
        self.id = id
        self.name = name
        self.exp = exp


class Films():
    def __init__(self, id, name, year, director):
        self.id = id
        self.name = name
        self.year = year
        self.director = director


class Participation():
    def __init__(self, idA, idF):
        self.idA = idA
        self.idF = idF



d = open('Актеры.txt').read()
e = []
e = d.split(' \\t ')
act = []
for i in range(len(e)):
    c = str(e[i])
    x = ''
    for m in range(len(c)):
        if c[m] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
            x += c[m]
        else:
            break
    z = ''
    for n in range(len(x), len(c)):
        if c[n] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
            g = n
            z += c[n]
    y = c[len(x) + 1:(g-len(z))]
    act.append(Actors(x, y, z))


f = open('Фильмы.txt').read()
h = []
h = f.split(' \\t ')
films = []
for i in range(len(h)):
    c = str(h[i])
    x = ''
    for m in range(len(c)):
        if c[m] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
            x += c[m]
        else:
            break
    w = ''
    for t in range(len(c)- 1, -1, -1):
        if c[t] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
            w += c[t]
            t1 = t
        else:
            break
    z = ''
    for n in range(t1 - 1, -1, -1):
        if c[n] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
            z += c[n - 3]
            z += c[n - 2]
            z += c[n - 1]
            z += c[n]
            g = n
            break
    y = c[len(x) + 1:g - 4]
    films.append(Films(x, y, z, w))


j = open('Участие в фильмах.txt').read()
k = []
k = j.split(' \\t ')
part = []
for i in range(len(k)):
    c = str(k[i])
    f = []
    f = c.split(' ')
    x = f[1]
    y = f[0]
    part.append(Participation(x, y))


a = open('Режиссеры.txt').read()
b = []
b = a.split(' \\t ')
dir = []
for i in range(len(b)):
    c = str(b[i])
    z = 0
    x = ''
    for m in range(len(c)):
        if c[m] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
            x += c[m]
        else:
            break
    for n in films:
        if n.director == x:
            z += 1
    y = c[len(x) + 1:]
    dir.append(Directors(x, y, z))


print('Выберете задачу:\n1 ) Какие фильмы снял режиссер до выбранного года\n2 ) Какие актеры после выбранного года снимались только у одного режиссера\n3 ) У каких режиссеров кол-во фильмов равно кол-ву разных актеров в них')
ch = int(input())


def DirectorCh2(x):
    if int(x.director) == ch1 and int(x.year) <= year:
        return x.name


if ch == 1:
    print('Выберете режиссера:')
    for i in range(len(dir)):
        print(dir[i].id, ')', dir[i].name)
    ch1 = int(input())
    print('До какого года:')
    year = int(input())
    res = list(map(DirectorCh2, films))
    res = filter(None, res)
    print('\n'.join(res))


elif ch == 2:
    print('Введите год:')
    ch1 = int(input())
    for a in act:
        u = []
        for p in part:
            for f in films:
                if int(f.year) >= ch1:
                    if p.idF == f.id:
                        for d in dir:
                            if f.director == d.id and p.idA == a.id and d.name not in u:
                                u.append(d.name)
        if len(u) == 1:
            print(a.name)

elif ch == 3:
    c = []
    for f in films:
        v = []
        for p in part:
            if p.idF == f.id:
                x = p.idA
                for a in act:
                    if a.id == x and a.name not in v:
                        v.append(a.name)
        d = f.director
        for y in dir:
            if d == y.id:
                if y.mov == len(v) and y.name not in c:
                    c.append(y.name)
    print('\n'.join(c))