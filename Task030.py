from datetime import datetime


def decorator(f):
    def wrapper(*args):
        print((f.__name__, datetime.now()))
        return f(*args)

    return wrapper


@decorator
def sum(a, b):
    return a + b

print(sum(1, 2))