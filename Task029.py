def range2(a, b, c = 1):
    while a != b:
        yield a
        a += c

for i in range2(5, 1, -1):
    print(i)