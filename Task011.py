import math


def any_to_complex(f):
    def wrapper(first, second):
        if not isinstance(second, ComplexNumber):
            second = ComplexNumber(second)
        return f(first, second)

    return wrapper


class ComplexNumber:
    def __init__(self, real=0, imaginary=0):
        if isinstance(real, str):
            [realstr, imagstr] = real.split('+')
            real = int(realstr)
            imaginary = int(imagstr[:imagstr.find('i')])
        self.real = real
        self.imaginary = imaginary

    def __repr__(self):
        return "ComplexNumber('{}')".format(self)

    def __str__(self):
        return '%s + %si' % (self.real, self.imaginary)

    def __int__(self):
        return self.real

    def __abs__(self):
        return math.sqrt(self.real ** 2 + self.imaginary ** 2)

    length = __abs__

    def arg(self):
        if self.real > 0 and self.imaginary > 0:
            fi = math.atan(self.real / self.imaginary)
        if self.real > 0 > self.imaginary:
            fi = -math.atan(self.real / self.imaginary)
        if self.real < 0 < self.imaginary:
            fi = math.pi - math.atan(self.real / self.imaginary)
        if self.real < 0 and self.imaginary < 0:
            fi = -math.pi + math.atan(self.real / self.imaginary)
        if self.real == 0:
            if self.imaginary > 0:
                fi = math.pi / 2
            else:
                fi = -math.pi / 2
        if self.imaginary == 0:
            if self.real > 0:
                fi = 0
            else:
                fi = math.pi
        return fi

    def __pow__(self, power):
        return ComplexNumber(math.cos(power * self.arg()), math.sin(power * self.arg())) * abs(self) ** power

    @any_to_complex
    def __add__(self, other):
        return ComplexNumber(self.real + other.real, self.imaginary + other.imaginary)

    @any_to_complex
    def __sub__(self, other):
        return ComplexNumber(self.real - other.real, self.imaginary - other.imaginary)

    @any_to_complex
    def __mul__(self, other):
        return ComplexNumber(self.real * other.real - self.imaginary * other.imaginary,
                             self.real * other.imaginary + self.imaginary * other.real)

    @any_to_complex
    def __truediv__(self, other):
        r = other.real ** 2 + other.imaginary ** 2
        return ComplexNumber((self.real * other.real + self.imaginary * other.imaginary) / r,
                             (self.imaginary * other.real - self.real * other.imaginary) / r)