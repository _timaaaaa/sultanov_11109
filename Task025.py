import random
import time

def linear_search(thelist, x):
    start = time.time()
    for i in thelist:
        if x == i:
            finish = time.time()
            sum = start - finish
            return True, '%.20f' % sum

def built_in_search(thelist, x):
    start = time.time()
    val = x in thelist
    finish = time.time()
    sum = finish - start
    return val, '%.20f' % sum

def timsort_binsearch(thelist, x):
    sort_start = time.time()
    thelist.sort()
    sort_finish = time.time()
    sort_time = sort_finish - sort_start
    bipolar_start = time.time()
    mid = len(thelist) // 2
    low = 0
    high = len(thelist) - 1
    while thelist[mid] != x and low <= high:
        if x > thelist[mid]:
            low = mid + 1
        else:
            high = mid - 1
        mid = (low + high) // 2
    if low > high:
        val = False
    else:
        val = True
    bipolar_end = time.time()
    bipolar_time = bipolar_end - bipolar_start
    return val, '%.20f' % sort_time, '%.20f' % bipolar_time


if __name__ == '__main__':
    lst = [int(i * random.random()) for i in range(10 ** 5)]
    x = 10


print(lst, x)